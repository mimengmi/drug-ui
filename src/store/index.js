import { createStore } from "vuex";
import createPersistedstate from "vuex-persistedstate";

//初始state
const defaultStore = () => {
  return {
    //用户对象
    user: {},
    //token
    token: "",
    //菜单状态
    isHideMenu: false,
    //当前菜单位置
    menuSeat: "/home",
    //tabs对象
    tabsData: []
  }
}

//用于存储数据
const state = defaultStore();
//用于响应组件中的动作
const actions = {
  //context上下文对象，value值
  update_user(context, value) {
    context.commit("UPDATE_USER", value);
  },
  //修改token
  update_token(context, value) {
    context.commit("UPDATE_TOKEN", value);
  },
  //改变侧边菜单隐藏或显示
  update_menu(context, value) {
    context.commit("UPDATE_MENU", value);
  },
  //改变菜单当前位置
  update_menuSeat(context, value) {
    context.commit("UPDATE_MENU_SEAT", value);
  },
  //增加tabs
  add_tabs(context, value) {
    context.commit("ADD_TABS", value);
  },
  //删除tabs
  delete_tabs(context, value) {
    //先清空数组
    context.state.tabsData.length = 0;
    context.commit("DELETE_TABS", value);
  },
  //重置state
  reset_state(context) {
    context.commit("RESET_STATE");
  }
};

//用于操作数据（state）
const mutations = {
  //登录时保存用户对象
  UPDATE_USER(state, value) {
    state.user = value;
  },
  UPDATE_TOKEN(state, value) {
    state.token = value;
  },
  UPDATE_MENU(state, value) {
    state.isHideMenu = value;
  },
  UPDATE_MENU_SEAT(state, value) {
    state.menuSeat = value;
  },
  ADD_TABS(state, value) {
    state.tabsData.push(value)
  },
  DELETE_TABS(state, value) {
    state.tabsData.push(...value);
  },
  RESET_STATE(state) {
    Object.assign(state, defaultStore())
  }
};

//暴露
export default createStore({
  state,
  mutations,
  actions,
  modules: {},
  //vuex数据持久化
  plugins: [
    createPersistedstate({
      storage: window.sessionStorage,
      reducer(val) {
        //只存储state的值
        return {
          //用户对象
          user: val.user,
          //token
          token: val.token,
          isHideMenu: val.isHideMenu,
          menuSeat: val.menuSeat,
          tabsData: val.tabsData
        };
      }
    })
  ]
});

