import { createRouter, createWebHistory } from "vue-router";

const routes = [
  // 匹配找不到的所有页面
  {
    path: '/:catchAll(.*)',
    name: "404",
    component: () => import("../views/404")
  }, {
    path: "/",
    // 重定向到登录
    redirect: "/login"
  },
  //登录
  {
    path: "/login",
    name: "Login",
    component: () => import("../views/Login.vue")
  }, {
    path: "/layout", name: "Layout", component: () => import("../components/layout"), children: [
      //主页
      {
        path: "/home",
        name: "Home",
        component: () => import("../views/Home.vue")
      },
      // 个人中心
      {
        path: "/personal",
        name: "Personal",
        component: () => import("../views/Personal.vue")
      },
      //用户管理
      {
        path: "/user",
        name: "User",
        component: () => import("../views/User.vue")
      },
      //菜单管理
      {
        path: "/menu",
        name: "Menu",
        component: () => import("../views/Menu.vue")
      },
      //角色管理
      {
        path: "/role",
        name: "Role",
        component: () => import("../views/Role.vue")
      },
      //登录日志
      {
        path: "/loginLog",
        name: "LoginLog",
        component: () => import("../views/log/LoginLog.vue")
      },
      //操作日志
      {
        path: "/optLog",
        name: "OptLog",
        component: () => import("../views/log/OptLog.vue")
      },
      //部门
      {
        path: "/dept",
        name: "Dept",
        component: () => import("../views/Dept.vue")
      },
      //公告
      {
        path: "/notice",
        name: "Notice",
        component: () => import("../views/Notice.vue")
      },
      //关于系统
      {
        path: "/about",
        name: "About",
        component: () => import("../views/About.vue")
      }, {
        path: "/supplier",
        name: "Supplier",
        component: () => import("../views/drug/Supplier.vue")
      }, {
        path: "/drug",
        name: "Drug",
        component: () => import("../views/drug/Drug.vue")
      }, {
        path: "/drugEnter",
        name: "DrugEnter",
        component: () => import("../views/drug/DrugEnter.vue")
      }, {
        path: "/drugOut",
        name: "DrugOut",
        component: () => import("../views/drug/DrugOut.vue")
      }, {
        path: "/stock",
        name: "Stock",
        component: () => import("../views/drug/Stock.vue")
      }, {
        path: "/return",
        name: "Return",
        component: () => import("../views/drug/Return.vue")
      }, {
        path: "/indent",
        name: "Indent",
        component: () => import("../views/drug/Indent.vue")
      }

    ]
  },
];

//创建router
const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

//暴露router
export default router;
