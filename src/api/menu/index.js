import request from "@/utils/request";

//条件查询
export function getMenu(name) {
    return request({
        url: '/perms/selectPerms',
        method: 'get',
        params: {
            name
        }
    })
}