import request from "@/utils/request";


//查询状态为正常的信息
export function selectNoticeByStatus() {
    return request({
        url: "/notice/selectNoticeByStatus",
        method: "get",
    })
}