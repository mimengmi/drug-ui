import axios from "axios";
import store from "@/store";
import { ElMessage } from "element-plus";
import router from "@/router";

//是否提示消息
let isNews = false;
const request = axios.create({
  //服务器访问地址
  baseURL: "/api",
  //过去时间
  // timeout: 5000
});
//请求拦截器
request.interceptors.request.use(function (config) {
  let user = store.state.user;
  if (user.account) {
    //每次请求携带token
    config.headers["token"] = store.state.token;
  }
  return config;
}, function (error) {
  //失败
  return Promise.reject(error);
});


// 响应拦截器
request.interceptors.response.use(
  response => {
    let res = response.data;
    //被拦截时的响应
    if (res.code === 403) {
      if (!isNews) {
        ElMessage.error(res.msg);
      }
      //清除所有vuex的值
      store.dispatch("reset_state")
      //清空session
      sessionStorage.clear();
      //路由跳转
      router.replace({ path: "/" });
      isNews = true;
      return;
    }

    // 如果是返回的文件
    if (response.config.responseType === "blob") {
      isNews = true;
      return res;
    }
    // 兼容服务端返回的字符串数据
    if (typeof res === "string") {
      isNews = true;
      res = res ? JSON.parse(res) : res;
    }
    return res;
  },
  error => {
    //失败
    return Promise.reject(error);
  }
);

// 以request暴露出去
export default request;
