import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
//汉化
import locale from 'element-plus/dist/locale/zh-cn.mjs'
//创建实例
const app = createApp(App)
//vuex
app.use(store)
//路由
app.use(router)
//ElementPlus
app.use(ElementPlus,{locale})
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}
//挂载
app.mount('#app');
