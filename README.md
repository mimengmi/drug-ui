## 安装依赖包
```
npm install
```

### 运行
```
npm run serve
```

### 后端代码
```
https://gitee.com/mimengmi/drug-serve
```

### 打包
```
npm run build:prod
```

