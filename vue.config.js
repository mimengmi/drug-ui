const { defineConfig } = require("@vue/cli-service");
//获取ip
const address = require('address')
module.exports = defineConfig({
  devServer: {
    //host
    host: address.ip() || 'localhost',
    //端口
    port: 8080,
    //是否自动打开浏览器
    open: true,
    proxy: {//设置代理，必须填
      '/api': { //设置拦截器  拦截器格式   斜杠+拦截器名字，名字可以自己定
        target: process.env.VUE_APP_SERVER_URL, //代理的目标地址
        changeOrigin: true,  //是否设置同源，输入是的
        pathRewrite: {      //路径重写
          '^/api': ''    //选择忽略拦截器里面的内容
        }
      }
    }
  },
  transpileDependencies: true,
  // 关闭eslint校验
  lintOnSave: false,
});
